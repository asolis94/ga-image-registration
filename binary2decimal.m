function r=binary2decimal(n) 
    isNegative = false;
    if(n(1) == 1)
       n(1) = 0;
       isNegative = true;
    end

    r=0;
    tam=size(n,2);

    for i=tam:-1:1
      r=r+n(i)*2.^(tam-i); 
    end

    if(isNegative)
       r = r * -1; 
    end
end
