function r=decimal2binary(num,m)
    r = zeros(1,m);
    decimalnum = num;
    for i=m:-1:1
        r(i)= mod(num,2);
        num = fix(num./2);
    end
    
    if(decimalnum < 0)
       r(1) = 1; 
    end
end