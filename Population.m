function [binPopulation] = Population(n, range, dof)
    p = randi(range,n,dof);

    %Magnitude is the max number of bits needed for represent the max
    %number in our population, since we are using a bit for negative
    %numbers it is necessary to add 1 bit.
    %magn = length(dec2bin(max(range)));
    % 3 bits for representate the decimal number 5.
    % 1 bit for representate the negative number.
    magn = 3 + 1;

    binPopulation = zeros()
        
end