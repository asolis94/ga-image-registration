function binOutline = getOutline(grayImage)

    %Sobel Mask
    kernel = [1 2 1; 0 0 0; -1 -2 -1];

    % 2D Convolution
    dX = conv2(grayImage, kernel , 'same');   
    dY = conv2(grayImage, kernel', 'same');
    outline = sqrt(dX.^2 + dY.^2);
    
    % Thresholding 
    threshold = 2 * mean(outline(:));
    binOutline = (outline > threshold);