function cameraVertrxes = getProjection(rawVertrxes,worldToCamera)
   
    cameraVertrxes = rawVertrxes*worldToCamera;
    
    if(cameraVertrxes(4)~=1)
        cameraVertrxes(1) = cameraVertrxes(1)/cameraVertrxes(4);
        cameraVertrxes(2) = cameraVertrxes(2)/cameraVertrxes(4);
        cameraVertrxes(3) = cameraVertrxes(3)/cameraVertrxes(4);
    end
