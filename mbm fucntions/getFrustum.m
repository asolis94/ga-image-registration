function frustum = getFrustum(bottofrustum,top,left,rigth,nearClippingPlane,farClippingPlane)
    
    frustum = zeros(4,4);  
    frustum(1,1) = 2*nearClippingPlane/(rigth-left);
    frustum(2,2) = 2*nearClippingPlane/(top-bottofrustum);
    frustum(1,3) = (rigth+left)/(rigth-left);
    frustum(3,2) = (top+bottofrustum)/(top-bottofrustum);
    frustum(3,3) = -(farClippingPlane+nearClippingPlane)/(farClippingPlane-nearClippingPlane);
    frustum(3,4) = -1;
    frustum(4,3) = -2*farClippingPlane*nearClippingPlane/(farClippingPlane-nearClippingPlane);
