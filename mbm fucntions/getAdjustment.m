function adjustmentMatrix = getAdjustment(referenceOutline,vertexs,faces,rotationMatrix,randomDistribution)
 
    k =1; th = 50; maxQuality = 55;
    while true && k<5 && maxQuality>th
    
        th = maxQuality;
        if k ==1        
            for n=1:1:203
                eulerAngles1 = randomDistribution(n,1,k) *(pi/180);
                eulerAngles2 = randomDistribution(n,2,k) *(pi/180);
                eulerAngles3 = randomDistribution(n,3,k) *(pi/180);

                translationMatrix = [randomDistribution(n,4,k),randomDistribution(n,5,k),randomDistribution(n,6,k)];
                refinementMatrix = rotationMatrix*(eul2rotm([eulerAngles1,eulerAngles2,eulerAngles3]));

                [binOutline,binArea] = getTarget(vertexs,faces,refinementMatrix,translationMatrix,ones(1,1));  

                T.Quality{n} = getRefinement(binOutline,binArea,referenceOutline);
                T.Rotation{n} = refinementMatrix;
                T.Translation{n} = translationMatrix;       
            end       
            [~,index] = sort(cell2mat(T.Quality));
            processingDistribution = [(randomDistribution(index(end-round(203/2)+1:end),:,k)+randomDistribution(1:102,:,k+1)) ; randomDistribution(103:end,:,k+1)];        
        else 
            for n=1:1:203

                eulerAngles1 = processingDistribution(n,1) *(pi/180);
                eulerAngles2 = processingDistribution(n,2) *(pi/180);
                eulerAngles3 = processingDistribution(n,3) *(pi/180);

                translationMatrix = [processingDistribution(n,4),processingDistribution(n,5),processingDistribution(n,6)];
                refinementMatrix = rotationMatrix*(eul2rotm([eulerAngles1,eulerAngles2,eulerAngles3]));

                [binOutline,binArea] = getTarget(vertexs,faces,refinementMatrix,translationMatrix,ones(1,1));  

                T.Quality{n} = getRefinement(binOutline,binArea,referenceOutline);
                T.Rotation{n} = refinementMatrix;
                T.Translation{n} = translationMatrix;                    
            end 
            [~,index] = sort(cell2mat(T.Quality));
            processingDistribution = [(processingDistribution(index(end-round(203/2)+1:end),:)+randomDistribution(1:102,:,k+1)) ; randomDistribution(103:end,:,k+1)];
        end        
        [maxQuality,~] = max(cell2mat(T.Quality));       
        if maxQuality>th
            [~,refinementIndex] = max(cell2mat(T.Quality)); 
           refinedRotation = cell2mat(T.Rotation(refinementIndex));
           refinedTranslation = cell2mat(T.Translation(refinementIndex)); 
        end
        k = k+1;
        
    end   
    adjustmentMatrix = [refinedRotation, refinedTranslation'];