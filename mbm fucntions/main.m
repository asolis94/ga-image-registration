%% Model Based Matching
% Documentation of the Model Based Matching MATLAB version.

%% Clear stored memory
clear all;
close all;
clc;

%% Camera Params
Camera = webcam(1);
Camera.Resolution = '1280x720';

% Load camera instrisic params and random sampling for the pose
% refinement step
load cameraParams.mat
load randomDistribution.mat

%% Video player object to show webcam frames at 720p in RGB.
VideoPlayer = vision.VideoPlayer('Position',[100, 100, 1400, 800]);

%% Desired quality to get a match.
threshold = 80;

%% Morpholigical Structural Element used for dilation process, a 3x3 kernel.
structuralElement = ones(3,3);

%% Get Dataset.

%obj = readObj('3D Models\IMPart.obj');
%save('obj.mat','obj')
load obj.mat
vertexs = (obj.v);
faces = obj.f.v;
    
fileID = fopen('initialMatrix.txt','r');
tline = fgetl(fileID);
index = 1; k = 1;
while ischar(tline)

    if index >= 5
        matrixRAW(k,:) = str2num(tline);
        k = k +1;
    end

    index = index + 1; 
    tline = fgetl(fileID);

end
fclose(fileID);
rotationMatrix = matrixRAW(1:3,1:3);

structuralElement = ones(1,1);
[binOutline,binArea] = getTarget(vertexs,faces,rotationMatrix, [0,0,0], structuralElement);  

%% Model Based Matching
signalIndex = 1;
structuralElement = ones(21,21);
while true
    % Undistort and get the current frame
    undistortFrame = undistortImage(snapshot(Camera),cameraParams,'OutputView','same');
    
    % Model Based Matching Function
    [status, matchQuality,currentOutline] = getMatching(binOutline,binArea,structuralElement,undistortFrame,threshold);
   
    if(status)
        step(VideoPlayer, undistortFrame+uint8(binArea*255))
        
        % Pose refinement
        refinementMatrix = getAdjustment(currentOutline,vertexs,faces,rotationMatrix,randomDistribution);
        [binOutline,binArea] = getTarget(vertexs,faces,refinementMatrix(:,1:3),refinementMatrix(:,4)',ones(1,1));   
        
        figure()
        imshow(undistortFrame+uint8(binArea*255))
     
        return;
    end

    step(VideoPlayer, undistortFrame+uint8(binOutline*255))
                
end