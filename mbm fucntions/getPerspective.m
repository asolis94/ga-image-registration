function [rigth,left,bottom,top] = getPerspective(angleOfView,imageAspectRatio,nearClippingPlane)
    
    scale = tan(angleOfView*0.5*pi/180)*nearClippingPlane;
    rigth = imageAspectRatio*scale;
    left = -rigth;
    top = scale;
    bottom = -top;

