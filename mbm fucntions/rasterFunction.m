function xyRaster = rasterFunction(tranformedVertexs,translationMatrix)

    %Raster resolution
    imageWidth = 1280;
    imageHeight = 720;

    %Object position
    worldToCamera = eye(4,4);
    worldToCamera(4,1) = translationMatrix(1);
    worldToCamera(4,2) = translationMatrix(2);
    worldToCamera(4,3) = -300+translationMatrix(3);

    angleOfView = 43.3;
    nearClippingPlane = 0.1;
    farClippingPlane = 1000;

    imageAspectRatio = imageWidth/imageHeight;

    % Set the screen coordinates
    [rigth,left,bottom,top] = getPerspective(angleOfView,imageAspectRatio,nearClippingPlane);

    % Set the perspective projection matrix
    frustum = getFrustum(bottom,top,left,rigth,nearClippingPlane,farClippingPlane);

    % Loop over all points
    tranformedVertexs = tranformedVertexs-mean(tranformedVertexs);
    for i=1:length(tranformedVertexs)

        % Transform to camera space
        [vertCamera] = getProjection([tranformedVertexs(i,:),1], worldToCamera);

        % Project
        [projectedVert] = getProjection(vertCamera, frustum);

        if (projectedVert(1) < -imageAspectRatio || projectedVert(1) > imageAspectRatio || projectedVert(2) < -1 || projectedVert(2) > 1)
            continue;
        end

        % Convert to raster space and mark the position of the vertex in the image with a simple dot
        x = min(imageWidth - 1, uint32(((projectedVert(1) + 1) * 0.5 * imageWidth)));
        y = min(imageHeight - 1, uint32(((1 - (projectedVert(2) + 1) * 0.5) * imageHeight)));
        xyRaster(i,:) = double([(x+1),(y+1),0]);
    end