function xyArea = getArea(xyRaster,faces,binArea)  

    for i=1:1:length(faces)

            iv(1,:) = xyRaster(faces(i,1),:);
            iv(2,:) = xyRaster(faces(i,2),:);
            iv(3,:) = xyRaster(faces(i,3),:);

            maxX = max(iv(:,2));
            minX = min(iv(:,2));

            maxY = max(iv(:,1));
            minY = min(iv(:,1));

            for u=minX:1:maxX
                for v=minY:1:maxY

                    if getSurfaces(iv,v,u) == true
                        binArea(u,v) = 1;
                    end

                end
            end
    end
    xyArea = binArea;
