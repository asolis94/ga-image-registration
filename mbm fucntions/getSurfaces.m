function validation = getSurfaces(iv,u,v)  

    validation1 = ((v-iv(1,2))*(iv(2,1)-iv(1,1)))-((u-iv(1,1))*(iv(2,2)-iv(1,2))) >=-10;
    validation2 = ((v-iv(2,2))*(iv(3,1)-iv(2,1)))-((u-iv(2,1))*(iv(3,2)-iv(2,2))) >=-10;
    validation3 = ((v-iv(3,2))*(iv(1,1)-iv(3,1)))-((u-iv(3,1))*(iv(1,2)-iv(3,2))) >=-10;
    validation = validation1 && validation2 && validation3;
