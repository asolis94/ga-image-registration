function [binOutline,binArea] = getTarget(vertexs,faces,rotationMatrix,translationMatrix,structuralElement)  

    vertexs(:,1) = vertexs(:,1);
    vertexs(:,2) = vertexs(:,2);
    vertexs(:,3) = vertexs(:,3);

    for k =1:1:length(vertexs)
        tranformedVertexs(k,:) = vertexs(k,:)*(rotationMatrix);
    end

    % Get rastered vertexs.
    xyRaster = rasterFunction(tranformedVertexs,[rotationMatrix;translationMatrix]);  
  
    % Get area.
    binArea = zeros(720,1280);   
    binArea = getArea(xyRaster,faces,binArea);
    
    % Get the model outline.
    modelOutline = getOutline(binArea);

    % Dilate the model outline.
    binOutline = imdilate(modelOutline, structuralElement);