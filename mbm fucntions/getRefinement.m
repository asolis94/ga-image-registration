function refinedQuality = getRefinement(binOutline,binArea,matchedFrame)

    % Get number of points in outline and area images.
    pointsOutline = sum(sum(binOutline));
    pointsArea = sum(sum(binArea));
         
    % Get correlation between the webcam's feed and the template.
    borderMatchQuality = sum(sum(binOutline & matchedFrame));
    areaMatchQuality = sum(sum(binArea & imcomplement(matchedFrame)));

    borderMatchQuality = ((borderMatchQuality*100)/pointsOutline);
    areaMatchQuality = ((areaMatchQuality*100)/pointsArea);
    refinedQuality = (borderMatchQuality+areaMatchQuality)/2;