function [status,matchQuality,currentOutline] = getMatching(binOutline,binArea,structuralElement,currentFrame,threshold)

    % Get number of points in outline and area images.
    pointsOutline = sum(sum(binOutline));
    pointsArea = sum(sum(binArea));
   
   % Convert RGB Image to GrayScale.
    currentFrame = rgb2gray(currentFrame);

   % Get the current outline.
    currentOutline = getOutline(currentFrame);
  
    % Dilate the current outline.
    outlineDilatet = imdilate(currentOutline,structuralElement);
       
    % Get correlation between the webcam's feed and the template.
    borderMatchQuality = sum(sum(binOutline & outlineDilatet));
    areaMatchQuality = sum(sum(binArea & imcomplement(currentOutline)));

    borderMatchQuality = ((borderMatchQuality*100)/pointsOutline);
    areaMatchQuality = ((areaMatchQuality*100)/pointsArea);

    status  = 0;
    matchQuality = (borderMatchQuality+areaMatchQuality)/2;
    if(borderMatchQuality >= threshold && areaMatchQuality >= threshold)
        status = 1;
    end